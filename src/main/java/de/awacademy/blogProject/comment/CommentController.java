package de.awacademy.blogProject.comment;

import de.awacademy.blogProject.post.Post;
import de.awacademy.blogProject.post.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Controller
public class CommentController {

    private CommentService commentService;
    private PostService postService;

    @Autowired
    public CommentController(CommentService commentService, PostService postService) {
        this.commentService = commentService;
        this.postService = postService;
    }

    @GetMapping("/blog/comment/{postId}")
    public String comment(Model model, @PathVariable Integer postId) {
        Optional<Post> post = postService.getPostById(postId);
        if (!post.isPresent()) {
            return "redirect:/blog";
        }
        model.addAttribute("post", post.get());
        return "comment";
    }


}
