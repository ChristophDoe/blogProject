package de.awacademy.blogProject.comment;


import de.awacademy.blogProject.post.Post;
import de.awacademy.blogProject.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Entity
public class Comment {


    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDateTime dateTime;
    private String textComment;

//    @ManyToOne
//    private User user;


    public Comment() {
    }

    @ManyToOne
    @OrderBy("dateTime DESC")
    private Post post;          // Beziehung zu Post

    public int getId() {
        return id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getTextComment() {
        return textComment;
    }

//    public User getUserId() {
//        return user;
//    }

    public void setPost(Post post) {
        this.post = post;
    }
}
