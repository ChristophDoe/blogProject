package de.awacademy.blogProject.comment;


import de.awacademy.blogProject.post.Post;
import de.awacademy.blogProject.post.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;
    private PostRepository postRepository;


    public CommentService() {
    }


    public void add(Comment comment, Integer postId) {
        Optional<Post> post = postRepository.findById(postId);
        comment.setPost(post.get());
        commentRepository.save(comment);
    }

    public List<Comment> getCommentList() {
        return commentRepository.findAll();
    }


}






