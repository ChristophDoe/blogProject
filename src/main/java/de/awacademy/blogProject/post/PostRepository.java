package de.awacademy.blogProject.post;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostRepository extends CrudRepository<Post, Integer> {

    List<Post> findAll();
    List<Post> findAllByOrderByIdDesc ();
}
