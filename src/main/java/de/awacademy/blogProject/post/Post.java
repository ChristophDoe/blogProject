package de.awacademy.blogProject.post;

import de.awacademy.blogProject.comment.Comment;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String textPost;
    private LocalDateTime dateTime;

    @OneToMany (mappedBy = "post")
    @OrderBy("dateTime ASC")
    private List<Comment> commentList;

    public Post() {
    }

    public Post(String textPost, LocalDateTime dateTime) {
        this.textPost = textPost;
        this.dateTime = dateTime;
    }

    public int getId() {
        return id;
    }

    public String getTextPost() {
        return textPost;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }
}
