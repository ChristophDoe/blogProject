package de.awacademy.blogProject.post;


import de.awacademy.blogProject.session.LoginDTO;
import de.awacademy.blogProject.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PostController {

    private PostService postService;



    @Autowired
    public PostController (PostService postService){
        this.postService=postService;
    }

    @GetMapping("/")
    public String index(){
        return "redirect:/blog";
    }


    @GetMapping("/blog")
    public String index(Model model) {
    model.addAttribute("PostList", postService.getPosts());
    model.addAttribute("userDto", new UserDto("", ""));
    model.addAttribute("loginDTO",new LoginDTO("",""));
   // model.addAttribute("sessionUser")
        return "blog";
    }





}
