package de.awacademy.blogProject.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    private PostRepository postRepository;


    @Autowired
    public PostService (PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public List<Post> getPosts () {
        return postRepository.findAllByOrderByIdDesc();
    }

    // Methode bekommt Id und gibt Post zurück
    public Optional <Post> getPostById (int id) {
        return postRepository.findById(id);
    }


}
