package de.awacademy.blogProject.user;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class

UserDto {


    @NotEmpty
    private String userName;

    @NotEmpty
    @Size(min = 5)
    private String password;

    public UserDto(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
