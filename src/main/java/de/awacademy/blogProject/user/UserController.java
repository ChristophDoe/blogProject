package de.awacademy.blogProject.user;

import de.awacademy.blogProject.session.Session;
import de.awacademy.blogProject.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import de.awacademy.blogProject.session.LoginDTO;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.Instant;
import java.util.Optional;


@Controller
public class UserController {


    private UserRepository userRepository;
    private SessionRepository sessionRepository;

    @Autowired
    public UserController(UserRepository userRepository, SessionRepository sessionRepository) {
        this.userRepository = userRepository;
        this.sessionRepository=sessionRepository;
    }


    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("userDto") UserDto userDto, BindingResult bindingResult) {
        if (userRepository.existsByUserName(userDto.getUserName())) {
            bindingResult.addError(new FieldError("userDto", "username", "Username ist schon vergeben, bitte ändern"));
        }
        if (bindingResult.hasErrors()) {
            return "blog";
        }
        User user = new User(userDto.getUserName(), userDto.getPassword());
        userRepository.save(user);


        return "redirect:/blog";
    }

//    @PostMapping("/login")
//    public String login(@ModelAttribute("login") LoginDTO login, BindingResult bindingResult, HttpServletResponse response) {
//        Optional<User> optionalUser = userRepository.findByUserNameAndPassword(login.getUserName(), login.getPassword());
//
//        if (optionalUser.isPresent()) {
//            Session session = new Session(optionalUser.get(), Instant.now().plusSeconds(7*24*60*60));
//            sessionRepository.save(session);
//
//            Cookie cookie = new Cookie("sessionId", session.getId());
//            response.addCookie(cookie);
//            // Login erfolgreich
//            return "redirect:/blog";
//        }
//
//        bindingResult.addError(new FieldError("login", "password", "Login fehlgeschlagen."));
//
//        return "blog";
//    }

}


