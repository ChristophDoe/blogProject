package de.awacademy.blogProject.user;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


public interface UserRepository extends CrudRepository <User, Integer> {





  Optional<User> findByUserNameAndPassword (String userName, String password);
  boolean existsByUserName (String userName);           // Prüft Datenbank auf Username
    Optional <User> findByUserName (String userName);

}
